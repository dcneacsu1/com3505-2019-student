Dear Demetra Neacsu,

Here are your preliminary marks for COM3505 project lab assessment, based on your Git repo at
https://gitlab.com/dcneacsu1/com3505-2019-student/tree/68ec79665fb1182965a73bc2938cf2b7d4ed6c5a
and your lab demo.

Data indicates that your partner (if any) for this work was: Vasilka Zheleva.

Below details of what we found in your docs and code, followed by a summary of some of the data points we have for your work.

We have preliminarily evaluated your work to be lower second class (approx. 58.0%): that's good work, well done!

If you find anything factually incorrect (especially in relation to the Git commit number I've used, or who you were partnering with, please let me know ASAP.

All the best,
Hamish


This is a good project, a little light on working implementation but with good
documentation. Well done!

Notes from your demo:

- IR remote
- UI for 4 brands
- wifi config @ boot
- tried IFTTT notifications (not working)
- linux problems
- showed working with another team's receiver

We found reasonable evidence of some or all of the following:

- choice of viable project
- base functionality and extensions
- documentation
  - exhibits understanding of the field
  - comprehesive
  - concise
  - illustrations
  - references
  - clarity
  - basic requirements
  - enhancements made
  - possible future enhancements
  - specifics covered:
    - how secure is the implementation?
    - how efficient is the implementation?
    - are there [m]any bugs?!
    - how might it be improved?
    - pseudocode for the main algorithm?
- good code quality
  - structure, readability, maintainability
  - commenting and style
  - interfaces minimal, simplicity maximal
- process
  - gitlab records
  - GTA demo positive
  - code and docs in the right places
- testing
  - documentation of appropriate testing
  - implementation of testing
  - data collection routines
- teamwork



Git data point summary:


* 68ec796 -  (HEAD -> master, origin/master, origin/HEAD) Update MyProjectDocs.mkd (2020-02-12 11:20:22 +0000) <Demetra Neacsu>
* 38e83e7 -  Update MyProjectDocs.mkd (2020-02-12 11:10:21 +0000) <Demetra Neacsu>
* e688573 -  Update MyProjectDocs.mkd (2020-02-12 01:01:54 +0000) <Demetra Neacsu>
* 4c8e158 -  Update sketch.ino (2020-02-12 00:59:36 +0000) <Demetra Neacsu>
* c8c2168 -  Update RemoteFunctions.h (2020-02-12 00:56:41 +0000) <Demetra Neacsu>
* 76244fd -  Update RemoteButton.h (2020-02-12 00:55:21 +0000) <Demetra Neacsu>
* b39410c -  Update RemoteButton.cpp (2020-02-12 00:54:24 +0000) <Demetra Neacsu>
* 61e141a -  Update MyProjectDocs.mkd (2020-02-12 00:51:42 +0000) <Demetra Neacsu>
* 1d490f7 -  Delete IMG_50581.jpeg (2020-02-12 00:39:55 +0000) <Demetra Neacsu>
* 3700787 -  Upload New File (2020-02-12 00:39:25 +0000) <Demetra Neacsu>
* 2bd32ea -  Update MyProjectDocs.mkd (2020-02-12 00:37:23 +0000) <Demetra Neacsu>
* 19732ba -  Update MyProjectDocs.mkd (2020-02-12 00:15:06 +0000) <Demetra Neacsu>
* b15023c -  Added pics (2020-02-11 23:45:38 +0000) <acb17dcn@sheffield.ac.uk>
* e4615dd -  Upload New File (2020-02-11 23:43:34 +0000) <Demetra Neacsu>
* 8c2b265 -  Upload New File (2020-02-11 23:43:19 +0000) <Demetra Neacsu>
* 8a9318e -  Upload New File (2020-02-11 23:42:46 +0000) <Demetra Neacsu>
* 045a44f -  Upload New File (2020-02-11 23:42:27 +0000) <Demetra Neacsu>
* 2ca4289 -  Upload New File (2020-02-11 23:42:06 +0000) <Demetra Neacsu>
* b0bcab5 -  Upload New File (2020-02-11 23:41:21 +0000) <Demetra Neacsu>
* 7ddc14a -  Upload New File (2020-02-11 23:40:57 +0000) <Demetra Neacsu>
* 9e3fe5b -  Upload New File (2020-02-11 23:37:48 +0000) <Demetra Neacsu>
* 92e340c -  Add new directory (2020-02-11 23:37:06 +0000) <Demetra Neacsu>
*   af94ffd -  Fixed (2020-02-11 23:35:54 +0000) <acb17dcn@sheffield.ac.uk>
|\  
| * 30f8b48 -  Update MyProjectDocs.mkd (2020-02-11 23:34:49 +0000) <Demetra Neacsu>
| * 12e6a37 -  Update AdafruitIOFunctions.ino (2020-02-11 23:13:36 +0000) <Demetra Neacsu>
| * 7d15804 -  Update MyProjectDocs.mkd (2020-02-11 23:12:52 +0000) <Demetra Neacsu>
* | 12d62aa -  blah (2020-02-11 23:35:13 +0000) <acb17dcn@sheffield.ac.uk>
|/  
* 4bc2bb9 -  Changed project added doc (2020-02-11 22:28:29 +0000) <acb17dcn@sheffield.ac.uk>
* cfc08c9 -  New ver (2020-02-10 17:57:16 +0000) <acb17dcn@sheffield.ac.uk>
*   910949a -  Merge branch 'master' of gitlab.com:dcneacsu1/com3505-2019-student (2020-02-10 16:22:30 +0000) <acb17dcn@sheffield.ac.uk>
|\  
* | d6cdda0 -  Created remote (2020-02-10 16:21:18 +0000) <acb17dcn@sheffield.ac.uk>
|/  
* c9f6e9b -  added OTA firmware version 53 (2019-11-12 15:34:27 +0000) <acb17dcn@sheffield.ac.uk>
* 413cd8c -  added OTA firmware version 52 (2019-11-12 15:04:48 +0000) <acb17dcn@sheffield.ac.uk>
* e44acde -  added OTA firmware version 51 (2019-11-12 14:24:20 +0000) <acb17dcn@sheffield.ac.uk>
* ddb9d4a -  Update README.mkd (2019-11-11 11:05:45 +0000) <Demetra Neacsu>
* 3bbdb8b -  Update README.mkd (2019-11-11 00:31:14 +0000) <Demetra Neacsu>
* 9bba41e -  Update README.mkd (2019-11-11 00:22:40 +0000) <Demetra Neacsu>
* 40820eb -  Update README.mkd (2019-11-11 00:15:13 +0000) <Demetra Neacsu>
* 71c3eab -  Update README.mkd (2019-11-11 00:14:34 +0000) <Demetra Neacsu>
* 73a8dc1 -  firmware update (2019-11-11 00:00:02 +0000) <Demetra Neacsu>
*   259a0c4 -  Merge branch 'master' of gitlab.com:dcneacsu1/com3505-2019-student (2019-11-10 23:50:51 +0000) <acb17dcn@sheffield.ac.uk>
|\  
| * 225e7c3 -  Update README.mkd (2019-11-10 23:50:41 +0000) <Demetra Neacsu>
* |   36fdb86 -  Merge branch 'master' of gitlab.com:dcneacsu1/com3505-2019-student (2019-11-10 23:49:44 +0000) <acb17dcn@sheffield.ac.uk>
|\ \  
| |/  
| * c266ac2 -  Replace terminal.png (2019-11-10 23:36:47 +0000) <Demetra Neacsu>
| * 926284f -  Upload New File (2019-11-10 23:36:26 +0000) <Demetra Neacsu>
| * 0176726 -  Replace broadband_25.jpg (2019-11-10 23:30:14 +0000) <Demetra Neacsu>
| * b889093 -  Picture2 (2019-11-10 23:28:50 +0000) <Demetra Neacsu>
| * 7de53cc -  Delete broadband.jpg (2019-11-10 23:24:30 +0000) <Demetra Neacsu>
| * f92afe1 -  Picture2 (2019-11-10 23:24:05 +0000) <Demetra Neacsu>
| * 2a79d9a -  Upload New File (2019-11-10 23:06:12 +0000) <Demetra Neacsu>
* | 97a61ef -  final version (2019-11-10 23:49:33 +0000) <acb17dcn@sheffield.ac.uk>
* | 8787707 -  added OTA firmware version 50 (2019-11-10 23:48:10 +0000) <acb17dcn@sheffield.ac.uk>
|/  
* 2d44835 -  Updated code to fix mistake (2019-11-10 22:10:07 +0000) <acb17dcn@sheffield.ac.uk>
* b8bd605 -  added OTA firmware version 49 (2019-11-10 17:53:52 +0000) <acb17dcn@sheffield.ac.uk>
* 47213e0 -  added OTA firmware version 48 (2019-11-10 17:36:52 +0000) <acb17dcn@sheffield.ac.uk>
* 338801a -  added OTA firmware version 47 (2019-11-10 17:24:37 +0000) <acb17dcn@sheffield.ac.uk>
* e1d732b -  added OTA firmware version 45 (2019-11-10 16:45:06 +0000) <acb17dcn@sheffield.ac.uk>
* ea5beaa -  last version (2019-11-09 21:57:34 +0000) <acb17dcn@sheffield.ac.uk>
* 56db8c7 -  exercise 2 finished (2019-11-09 19:20:59 +0000) <acb17dcn@sheffield.ac.uk>
* 71d2270 -  added OTA firmware version 45 (2019-11-09 18:17:39 +0000) <acb17dcn@sheffield.ac.uk>
* ae7a190 -  added OTA firmware version 45 (2019-11-09 12:56:38 +0000) <acb17dcn@sheffield.ac.uk>
* d8898d4 -  task2 (2019-11-07 21:32:55 +0000) <acb17dcn@sheffield.ac.uk>
* 8ddec7f -  added OTA firmware version 45 (2019-11-06 21:50:11 +0000) <acb17dcn@sheffield.ac.uk>
* d1ffcd5 -  new (2019-11-06 19:55:08 +0000) <acb17dcn@sheffield.ac.uk>
* 7bc49a1 -  Wifi (2019-11-06 19:46:59 +0000) <acb17dcn@sheffield.ac.uk>
* 632bea2 -  added OTA firmware version 45 (2019-11-06 19:16:21 +0000) <acb17dcn@sheffield.ac.uk>
* f2deda4 -  Ex 09 (2019-11-06 19:03:42 +0000) <acb17dcn@sheffield.ac.uk>
* 744b6fa -  added OTA firmware version 44 (2019-11-05 15:19:26 +0000) <acb17dcn@sheffield.ac.uk>
* 530ef48 -  Message (2019-11-05 15:18:39 +0000) <acb17dcn@sheffield.ac.uk>
*   3616b61 -  Merge branch 'master' of gitlab.com:dcneacsu1/com3505-2019-student (2019-11-05 15:13:24 +0000) <acb17dcn@sheffield.ac.uk>
|\  
* | c504f7a -  added OTA firmware version 43 (2019-11-05 15:12:30 +0000) <acb17dcn@sheffield.ac.uk>
|/  
*   fc17934 -  Merge branch 'master' of gitlab.com:dcneacsu1/com3505-2019-student (2019-10-22 12:38:21 +0100) <acb17dcn@sheffield.ac.uk>
|\  
* | c0c4f4f -  Finished exercise (2019-10-21 20:00:22 +0100) <acb17dcn@sheffield.ac.uk>
|/  
* 04668c5 -  exercise3 (2019-10-21 17:45:02 +0100) <acb17dcn@sheffield.ac.uk>
* eed5cc7 -  Emails pair.txt (2019-10-05 11:48:08 +0000) <Demetra Neacsu>
* 7b75137 -  Add new file (2019-10-05 11:46:27 +0000) <Demetra Neacsu>
* 8477986 -  Initial commit (2019-09-30 01:37:47 +0000) <Demetra Neacsu>

