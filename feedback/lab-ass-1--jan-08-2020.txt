Dear Demetra Neacsu,

Here are your preliminary marks for the COM3505 assessment, based on your Git repo at
https://gitlab.com/dcneacsu1/com3505-2019-student/tree/4c084ad618cb0d7692bb1f72240d45f5c7b415a5
and your lab assessment demo.

Data indicates that your partner (if any) for this work was: Vasilka Zheleva.

Below details of what we found in your docs and code, followed by a summary of some of the data points we have for your work.

We have preliminarily evaluated your work to be third class (approx. 48%): let me know if you want to discuss.

If you find anything factually incorrect (especially in relation to the Git commit number I've used, or who you were partnering with, please let me know ASAP.

All the best,
Hamish


Good work! Note that provisioning and OTA are different things (the former
being to do with equiping the device with network credentials, the latter
about managing updates). Also, the style is mostly not functional (but a bit
of a crazy mixture of imperative, procedural and OO!).
We found good evidence of a reaonable number of:

- correct functionality and extensions
  - OTA configured properly
  - touch-based security
  - access point cache
  - use of IDF APIs
- testing
  - documentation of appropriate testing
  - implementation of testing
  - data collection routines
- documentation
  - exhibits understanding of the field
  - exhibits understanding of provisioning and OTA
  - clarity
  - comprehesive
  - concise
  - illustrations
  - references
  - enhancements made
  - possible future enhancements
    - power
    - security
    - accelerometer
  - specifics covered:
    - what is the purpose of provisioning? and (over-the-air) updating?
    - how has `MyPro+UpdThingIDF` chosen to implement these functions?
    - how secure is the implementation?
    - how efficient is the implementation?
    - are there [m]any bugs?!
    - how might it be improved?
    - what is the dominant programming style? what are its advantages and
      disadvantages?
    - pseudocode for the main algorithm
- good code quality
  - structure, readability, maintainability
  - commenting and style
  - interfaces minimal, simplicity maximal
- process
  - gitlab records
  - GTA demo positive
  - code and docs in the right places
- teamwork



Git data point summary:


* ddb9d4a -  Update README.mkd (2019-11-11 11:05:45 +0000) <Demetra Neacsu>
* 3bbdb8b -  Update README.mkd (2019-11-11 00:31:14 +0000) <Demetra Neacsu>
* 9bba41e -  Update README.mkd (2019-11-11 00:22:40 +0000) <Demetra Neacsu>
* 40820eb -  Update README.mkd (2019-11-11 00:15:13 +0000) <Demetra Neacsu>
* 71c3eab -  Update README.mkd (2019-11-11 00:14:34 +0000) <Demetra Neacsu>
* 73a8dc1 -  firmware update (2019-11-11 00:00:02 +0000) <Demetra Neacsu>
*   259a0c4 -  Merge branch 'master' of gitlab.com:dcneacsu1/com3505-2019-student (2019-11-10 23:50:51 +0000) <acb17dcn@sheffield.ac.uk>
|\  
| * 225e7c3 -  Update README.mkd (2019-11-10 23:50:41 +0000) <Demetra Neacsu>
* |   36fdb86 -  Merge branch 'master' of gitlab.com:dcneacsu1/com3505-2019-student (2019-11-10 23:49:44 +0000) <acb17dcn@sheffield.ac.uk>
|\ \  
| |/  
| * c266ac2 -  Replace terminal.png (2019-11-10 23:36:47 +0000) <Demetra Neacsu>
| * 926284f -  Upload New File (2019-11-10 23:36:26 +0000) <Demetra Neacsu>
| * 0176726 -  Replace broadband_25.jpg (2019-11-10 23:30:14 +0000) <Demetra Neacsu>
| * b889093 -  Picture2 (2019-11-10 23:28:50 +0000) <Demetra Neacsu>
| * 7de53cc -  Delete broadband.jpg (2019-11-10 23:24:30 +0000) <Demetra Neacsu>
| * f92afe1 -  Picture2 (2019-11-10 23:24:05 +0000) <Demetra Neacsu>
| * 2a79d9a -  Upload New File (2019-11-10 23:06:12 +0000) <Demetra Neacsu>
* | 97a61ef -  final version (2019-11-10 23:49:33 +0000) <acb17dcn@sheffield.ac.uk>
* | 8787707 -  added OTA firmware version 50 (2019-11-10 23:48:10 +0000) <acb17dcn@sheffield.ac.uk>
|/  
* 2d44835 -  Updated code to fix mistake (2019-11-10 22:10:07 +0000) <acb17dcn@sheffield.ac.uk>
* b8bd605 -  added OTA firmware version 49 (2019-11-10 17:53:52 +0000) <acb17dcn@sheffield.ac.uk>
* 47213e0 -  added OTA firmware version 48 (2019-11-10 17:36:52 +0000) <acb17dcn@sheffield.ac.uk>
* 338801a -  added OTA firmware version 47 (2019-11-10 17:24:37 +0000) <acb17dcn@sheffield.ac.uk>
* e1d732b -  added OTA firmware version 45 (2019-11-10 16:45:06 +0000) <acb17dcn@sheffield.ac.uk>
* ea5beaa -  last version (2019-11-09 21:57:34 +0000) <acb17dcn@sheffield.ac.uk>
* 56db8c7 -  exercise 2 finished (2019-11-09 19:20:59 +0000) <acb17dcn@sheffield.ac.uk>
* 71d2270 -  added OTA firmware version 45 (2019-11-09 18:17:39 +0000) <acb17dcn@sheffield.ac.uk>
* ae7a190 -  added OTA firmware version 45 (2019-11-09 12:56:38 +0000) <acb17dcn@sheffield.ac.uk>
* d8898d4 -  task2 (2019-11-07 21:32:55 +0000) <acb17dcn@sheffield.ac.uk>
* 8ddec7f -  added OTA firmware version 45 (2019-11-06 21:50:11 +0000) <acb17dcn@sheffield.ac.uk>
* d1ffcd5 -  new (2019-11-06 19:55:08 +0000) <acb17dcn@sheffield.ac.uk>
* 7bc49a1 -  Wifi (2019-11-06 19:46:59 +0000) <acb17dcn@sheffield.ac.uk>
* 632bea2 -  added OTA firmware version 45 (2019-11-06 19:16:21 +0000) <acb17dcn@sheffield.ac.uk>
* f2deda4 -  Ex 09 (2019-11-06 19:03:42 +0000) <acb17dcn@sheffield.ac.uk>
* 744b6fa -  added OTA firmware version 44 (2019-11-05 15:19:26 +0000) <acb17dcn@sheffield.ac.uk>
* 530ef48 -  Message (2019-11-05 15:18:39 +0000) <acb17dcn@sheffield.ac.uk>
*   3616b61 -  Merge branch 'master' of gitlab.com:dcneacsu1/com3505-2019-student (2019-11-05 15:13:24 +0000) <acb17dcn@sheffield.ac.uk>
|\  
* | c504f7a -  added OTA firmware version 43 (2019-11-05 15:12:30 +0000) <acb17dcn@sheffield.ac.uk>
|/  
*   fc17934 -  Merge branch 'master' of gitlab.com:dcneacsu1/com3505-2019-student (2019-10-22 12:38:21 +0100) <acb17dcn@sheffield.ac.uk>
|\  
* | c0c4f4f -  Finished exercise (2019-10-21 20:00:22 +0100) <acb17dcn@sheffield.ac.uk>
|/  
* 04668c5 -  exercise3 (2019-10-21 17:45:02 +0100) <acb17dcn@sheffield.ac.uk>
* eed5cc7 -  Emails pair.txt (2019-10-05 11:48:08 +0000) <Demetra Neacsu>
* 7b75137 -  Add new file (2019-10-05 11:46:27 +0000) <Demetra Neacsu>
* 8477986 -  Initial commit (2019-09-30 01:37:47 +0000) <Demetra Neacsu>

