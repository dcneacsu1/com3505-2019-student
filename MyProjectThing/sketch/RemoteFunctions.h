// RemoteFunctions.h
// core definitions and includes

#ifndef REMOTEFUNCTIONS_H
#define REMOTEFUNCTIONS_H

// This method completes an action based on which tile has been selected and whether
// it has been selected for management
void doRemoteAction(RemoteButton*, bool);

#endif
