// IRCodes.h
// header file for getting the appropriate IR codes

#ifndef IRCODES_H
#define IRCODES_H

unsigned long getPowerCode(char*);
unsigned long getChanDownCode(char*);
unsigned long getChanUpCode(char*);
unsigned long getVolUpCode(char*);
unsigned long getVolDownCode(char*);
    
#endif
