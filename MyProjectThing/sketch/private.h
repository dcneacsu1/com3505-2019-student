// private.h
#ifndef PRIVATE_H
#define PRIVATE_H

#define _GITLAB_ID              "mygitlabname"
#define _GITLAB_TOKEN           "3xwxnjERrMoz-TbjwEQ-" // for private repos
#define _GITLAB_PROJ_ID         "14564237"
#define _DEFAULT_AP_KEY         "dumbpasskey"
#define _ELF_KEY                "AP passkey for Elves"
#define _ELF_AP_KEY             "AP passkey for Elves" // WaterElf AP key.
#define _CITSCI_IP              "IP address of Elf data collector"
#define _LORA_APP_KEY           "LoRaWAN app key"
#define _LORA_NET_KEY           "LoRaWAN network key"
#define _LORA_DEV_ADDR          "LoRaWAN device address"
#define _TIME_BETWEEN_WIFI_SCAN  50
#endif
