#include "unphone.h"

// calibration data for converting raw touch data to the screen coordinates
#define TS_MINX 3800
#define TS_MAXX 100
#define TS_MINY 100
#define TS_MAXY 3750

// This listens to button calls on the screen
void listenToTileList(uint16_t x, uint16_t y, RemoteButton** RemoteButtons, bool inManage) {
  uint16_t startingY = TS_MAXY - border * verticalMultiplier; // Initialise Y with a border
  uint16_t buttonHeight = (HX8357_TFTHEIGHT / rows - border) * verticalMultiplier; // Calculate button height based on rows
  uint16_t buttonWidth = (HX8357_TFTWIDTH / columns - border) * horizontalMultiplier; // Calculate button width based on columns

  // Iterate through the rows and columns
  for (int i = 0; i < rows; i++) {
    uint16_t startingX = TS_MINX - border * horizontalMultiplier; // Initialise X with a border
    for (int j = 0; j < columns; j++) {
      // If the user touches the screen on coordinates that belong to a button, the button's action is triggered
      uint16_t buttonXMargin1 = startingX;
      uint16_t buttonXMargin2 = startingX - buttonWidth - border;

      uint16_t buttonYMargin1 = startingY;
      uint16_t buttonYMargin2 = startingY - buttonHeight - border;
      
      if (x < buttonXMargin1 && x > buttonXMargin2 &&
          y < buttonYMargin1 && y > buttonYMargin2) {
        doRemoteAction(RemoteButtons[i*columns+j], inManage);
        return;
      }
      startingX -= buttonWidth - border * horizontalMultiplier; // Shift the x coordinate ready for the adjacent tile
    }
    startingY -= buttonHeight - border; // Shift the y coordinate ready for the adjacent tile
  }
}
