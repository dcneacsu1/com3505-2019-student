// sketch.ino

#include "unphone.h"
#include "AdafruitIOFunctions.h"
#include "RemoteButton.h"
#include "DrawingScreen.h"
#include "ScreenResponse.h"
#include "RemoteFunctions.h"
#include "IRCodes.h"
#include "RemoteCommands.h"

#include "private.h" // not for pushing; assumed to be at parent dir level
#include <ESPAsyncWebServer.h>
#include <IRremote.h>
#include <SD.h>
#include <list>

#define HX8357_TURQOAS   0x40E0D0
#define HX8357_PINK      0xFF5B77
#define HX8357_LIGHT_PEACH 0xde8906

// debugging infrastructure; setting different DBGs true triggers prints ////
#define dbg(b, s)       if(b) Serial.print(s)
#define dbf(b, ...)     if(b) Serial.printf(__VA_ARGS__)
#define dln(b, s)       if(b) Serial.println(s)
#define startupDBG      true
#define loopDBG         true
#define monitorDBG      true
#define netDBG          true
#define miscDBG         true
#define analogDBG       true
#define otaDBG          true

// functions
void draw();
void draw1();
void listenToScreen();
void updateRemoteButtons(char* newBrand);
void switchButtonsBrand(char* newBrand);
void setBrandChangeButton(char* newBrand);

////////////////////////////////////////////////////////////////////////////////////////////////////
// we're not in arduino land any more, so need to declare function protos ///
byte hextoi(char c);
void initWebServer();
void hndlRoot(AsyncWebServerRequest *);
void hndlNotFound(AsyncWebServerRequest *);
void hndlWifichz(AsyncWebServerRequest *);
void hndlStatus(AsyncWebServerRequest *);
void hndlWifi(AsyncWebServerRequest *);
void apListForm(String&);
void printIPs();
// OTA support //////////////////////////////////////////////////////////////
int firmwareVersion = 1;    // keep up-to-date! (used to check for updates)

// MAC address //////////////////////////////////////////////////////////////
char MAC_ADDRESS[13]; // MAC addresses are 12 chars, plus the NULL terminator
void getMAC1(char *);

int timeout = _TIME_BETWEEN_WIFI_SCAN;

// globals for a wifi access point and webserver ////////////////////////////
String apSSID = String("Pro+UpdThing-"); // SSID of the AP
String apPassword = _DEFAULT_AP_KEY;     // passkey for the AP
AsyncWebServer* webServer;               // async web server
String ip2str(IPAddress);                // helper for printing IP addresses
String cachedForm = "";                       // Cached wifi choosing form

// web server utils /////////////////////////////////////////////////////////
// the replacement_t type definition allows specification of a subset of the
// "boilerplate" strings, so we can e.g. replace only the title, or etc.
typedef struct { int position; const char *replacement; } replacement_t;
void getHtml(String& html, const char *[], int, replacement_t [], int);
#define ALEN(a) ((int) (sizeof(a) / sizeof(a[0]))) // only in definition scope!
#define GET_HTML(strout, boiler, repls) \
  getHtml(strout, boiler, ALEN(boiler), repls, ALEN(repls));

/////////////////////////////////////////////////////////////////////////////////////////////////

// Stores the iteration of the loop
int loopIteration = 0;

// Stores the last touch position as input by user
uint16_t lastX;
uint16_t lastY;

// Creates the RemoteButtons for each tile on the screen
RemoteButton* remoteButton1 = new RemoteButton();
RemoteButton* remoteButton2 = new RemoteButton();
RemoteButton* remoteButton3 = new RemoteButton();
RemoteButton* remoteButton4 = new RemoteButton();
RemoteButton* remoteButton5 = new RemoteButton();
RemoteButton* remoteButton6 = new RemoteButton();

// Stores the magnitude of the last accelerometer reading so that the difference between the two can
// be calculated
int lastMagnitude;
// Stores a stack of the most recent ten magnitudes
std::list<int> magnitudeStack;

// Main setup function
void setup() {
  
  Wire.setClock(100000); // higher rates trigger an IOExpander bug
  UNPHONE_DBG = true;
  Serial.begin(115200);  // init the serial line
  unPhone::begin();
  unPhone::checkPowerSwitch(); // check if power switch is now off

  //////////////////////////////////////////////////////////////////////////////////
  // OTA Update
  getMAC1(MAC_ADDRESS);          // store the MAC address
  apSSID.concat(MAC_ADDRESS);   // add the MAC to the AP SSID

  // For extra security, the first time you turn the remote on, it requires a wifi connection to update so you cannot run the remote on an older, less secure version.
  draw1();
  
  Serial.printf("doing wifi manager\n");
  joinmeManageWiFi(apSSID.c_str(), apPassword.c_str()); // get net connection
  Serial.printf("wifi manager done\n\n");

  // init the web server, say hello
  webServer = new AsyncWebServer(80);
  initWebServer();
  Serial.printf("firmware is at version %d\n", firmwareVersion);

  // check for and perform firmware updates as needed
  vTaskDelay(2000 / portTICK_PERIOD_MS); // let wifi settle
  joinmeOTAUpdate(
    firmwareVersion, _GITLAB_PROJ_ID,
    // "", // for publ repo "" works, else need valid PAT: _GITLAB_TOKEN,
    _GITLAB_TOKEN,
    "MyPro+UpdThingIDF%2Ffirmware%2F"
  );
  Serial.println("****************************************************");
  ///////////////////////////////////////////////////////////////////////////////

  // Initialises each of the RemoteButtons with some default value
  remoteButton1->init("Power", "LG", HX8357_RED);
  remoteButton2->init("Samsung", "LG", HX8357_LIGHT_PEACH);
  remoteButton2->setAdditionalText("Now LG");
  remoteButton3->init ("Vol up", "LG", HX8357_TURQOAS);
  remoteButton4->init("Ch up", "LG", HX8357_PINK);
  remoteButton5->init("Vol down", "LG", HX8357_TURQOAS);
  remoteButton6->init("Ch down", "LG", HX8357_PINK);
  
}

// This method updates each of the buttons with the new brand.
void updateRemoteButtons(char* newBrand) {
  switchButtonsBrand(newBrand);
  setBrandChangeButton(newBrand);
}

// Change the brand of all buttons to a new brand
void switchButtonsBrand(char* newBrand) {
  remoteButton1->setBrand(newBrand);
  remoteButton2->setBrand(newBrand);
  remoteButton3->setBrand(newBrand);
  remoteButton4->setBrand(newBrand);
  remoteButton5->setBrand(newBrand);
  remoteButton6->setBrand(newBrand);
}

// Change the brand button label to new brand
void setBrandChangeButton(char* newBrand) {
  if(newBrand == "Samsung") {
    remoteButton2->setColour(HX8357_BLUE);
    remoteButton2->setAdditionalText("Now Samsung");
    remoteButton2->setTitle("Sharp");
  } else if(newBrand == "Sharp") {
    remoteButton2->setColour(HX8357_MAGENTA);
    remoteButton2->setAdditionalText("Now Sharp");
    remoteButton2->setTitle("LG");
  } else if(newBrand == "LG") {
    remoteButton2->setColour(HX8357_LIGHT_PEACH);
    remoteButton2->setAdditionalText("Now LG");
    remoteButton2->setTitle("Panasonic");
  } else if(newBrand == "Panasonic"){
    remoteButton2->setColour(HX8357_GREEN);
    remoteButton2->setAdditionalText("Now Panason");
    remoteButton2->setTitle("Samsung");
  }
}

// Main loop
void loop() {
  int sliceSize = 500;
    
  // Draw the screen if this is the very first loop or if the user is in the scanning a remote signal
  // and a signal change has been detected.
  if (loopIteration == 0) {
    draw();
  }

  //////////////////////////////////////////////////////////
  timeout--; // timer between wifi checking
  if(loopIteration++ % sliceSize == 0) { // every sliceSize iterations
    dln(otaDBG, "OTA loop");
    printIPs();
  }
  vTaskDelay(100 / portTICK_PERIOD_MS); // 100 is min to allow IDLE on core 0
  /////////////////////////////////////////////////////////

  // Always listen for touches on the screen
  listenToScreen();
  loopIteration++;
}


// Get the RemoteButtons as an iterable collection
RemoteButton** getRemoteButtons() {
  return new RemoteButton*[6] {remoteButton1, remoteButton2, remoteButton3, remoteButton4, remoteButton5, remoteButton6};
}

// Draws the graphics on the screen
void draw() {
  // Clear screen
  unPhone::tftp->fillScreen(HX8357_BLACK);
  // Text Size and wrapping are standard, so they get set here ready for use across all drawing functions
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setTextWrap(true);
  // Draw graphics on dependent on what menu section is currently selected
  drawRemote(getRemoteButtons());
}

// Draws the waiting for connection screen
void draw1() {
  // Clear screen
  unPhone::tftp->fillScreen(HX8357_BLACK);
  // Text Size and wrapping are standard, so they get set here ready for use across all drawing functions
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setTextWrap(true);

  unPhone::tftp->setCursor(48, 86);
  unPhone::tftp->print("Waiting for Wifi Connection");
}

// Listens for touches on the screen
void listenToScreen() {
  uint16_t x, y;
  uint8_t z;
  if (unPhone::tsp->touched()) {
    // Read in the x, y and z values
    while (!unPhone::tsp->bufferEmpty()) {
      unPhone::tsp->readData(&x, &y, &z);
    }
    unPhone::tsp->writeRegister8(STMPE_INT_STA, 0xFF); // reset all ints

    // Calculate the total difference between the previous and current coordinates
    int totalDifference = lastX + lastY - x - y;
    // If there has been a z detection and a sizeable change of the difference, then
    // a touch has been made. This is to help cover double presses.
    if (z > 0 && (totalDifference > 100 || totalDifference < -100)) {
      // Listen to touch screen presses dependent on the menu section that the user is currently in
        listenToTileList(x, y, getRemoteButtons(), false);
      }
      // Update the previous touch coordinates
      lastX = x;
      lastY = y;
    }
}

// OTA STUFF ///////////////////////////////////////////////////////////////////////////////////////////////////////
// misc utilities
// get the ESP's MAC address
void getMAC1(char *buf) { // the MAC is 6 bytes, so needs careful conversion...
  uint64_t mac = ESP.getEfuseMac(); // ...to string (high 2, low 4):
  char rev[13];
  sprintf(rev, "%04X%08X", (uint16_t) (mac >> 32), (uint32_t) mac);

  // the byte order in the ESP has to be reversed
  for(int i=0, j=11; i<=10; i+=2, j-=2) {
    buf[i] = rev[j - 1];
    buf[i + 1] = rev[j];
  }
  buf[12] = '\0';
}
void printIPs() {
  dbg(startupDBG, "AP SSID: ");
  dbg(startupDBG, apSSID);
  dbg(startupDBG, "; IP address(es): local=");
  dbg(startupDBG, WiFi.localIP());
  dbg(startupDBG, "; AP=");
  dln(startupDBG, WiFi.softAPIP());
}

// web server utils /////////////////////////////////////////////////////////
void getHtml( // turn array of strings & set of replacements into a String
  String& html, const char *boiler[], int boilerLen,
  replacement_t repls[], int replsLen
) {
  for(int i = 0, j = 0; i < boilerLen; i++) {
    if(j < replsLen && repls[j].position == i)
      html.concat(repls[j++].replacement);
    else
      html.concat(boiler[i]);
  }
}
const char *templatePage[] = {    // we'll use Ex07 templating to build pages
  "<html><head><title>",                                                //  0
  "default title",                                                      //  1
  "</title>\n",                                                         //  2
  "<meta charset='utf-8'>",                                             //  3
  "<meta name='viewport' content='width=device-width, initial-scale=1.0'>\n"
  "<style>body{background:#FFF; color: #000; font-family: sans-serif;", //  4
  "font-size: 150%;}</style>\n",                                        //  5
  "</head><body>\n",                                                    //  6
  "<h2>Welcome to Thing!</h2>\n",                                       //  7
  "<!-- page payload goes here... -->\n",                               //  8
  "<!-- ...and/or here... -->\n",                                       //  9
  "\n<p><a href='/'>Home</a>&nbsp;&nbsp;&nbsp;</p>\n",                  // 10
  "</body></html>\n\n",                                                 // 11
};
void initWebServer() { // changed naming conventions to avoid clash with Ex06
  // register callbacks to handle different paths
  webServer->on("/", hndlRoot);              // slash
  webServer->onNotFound(hndlNotFound);       // 404s...
  webServer->on("/generate_204", hndlRoot);  // Android captive portal support
  webServer->on("/L0", hndlRoot);            // erm, is this...
  webServer->on("/L2", hndlRoot);            // ...IoS captive portal...
  webServer->on("/ALL", hndlRoot);           // ...stuff?
  webServer->on("/wifi", hndlWifi);          // page for choosing an AP
  webServer->on("/wifichz", hndlWifichz);    // landing page for AP form submit
  webServer->on("/status", hndlStatus);      // status check, e.g. IP address

  webServer->begin();
  dln(startupDBG, "HTTP server started");
}

// webserver handler callbacks
void hndlNotFound(AsyncWebServerRequest *request) {
  dbg(netDBG, "URI Not Found: ");
  dln(netDBG, request->url());
  request->send(200, "text/plain", "URI Not Found");
}
void hndlRoot(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page notionally at /");
  replacement_t repls[] = { // the elements to replace in the boilerplate
    {  1, apSSID.c_str() },
    {  8, "" },
    {  9, "<p>Choose a <a href=\"wifi\">wifi access point</a>.</p>" },
    { 10, "<p>Check <a href='/status'>wifi status</a>.</p>" },
  };
  String htmlPage = ""; // a String to hold the resultant page
  GET_HTML(htmlPage, templatePage, repls);
  request->send(200, "text/html", htmlPage);
}
void hndlWifi(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /wifi");
  int sliceSize = 200;

  String form = ""; // a form for choosing an access point and entering key
  if(cachedForm == "") {
    dln(otaDBG, "First time, checking for new wifi points.");
    apListForm(form);
  } else if(timeout <= 0) { // at least timeout iterations have passed
      dln(otaDBG, "Checking for new wifi points");
      timeout = _TIME_BETWEEN_WIFI_SCAN; // reset timeout
      apListForm(form);
  } else {
    dln(otaDBG, "Using cached wifi list.");
    Serial.printf("Timeout is %d\n", timeout);
    
    form = cachedForm;
  }
  
  replacement_t repls[] = { // the elements to replace in the boilerplate
    { 1, apSSID.c_str() },
    { 7, "<h2>Network configuration</h2>\n" },
    { 8, "" },
    { 9, form.c_str() },
  };
  String htmlPage = ""; // a String to hold the resultant page
  GET_HTML(htmlPage, templatePage, repls);

  request->send(200, "text/html", htmlPage);
}
void hndlWifichz(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /wifichz");

  String title = "<h2>Joining wifi network...</h2>";
  String message = "<p>Check <a href='/status'>wifi status</a>.</p>";

  String ssid = "";
  String key = "";
  for(uint8_t i = 0; i < request->args(); i++ ) {
    if(request->argName(i) == "ssid")
      ssid = request->arg(i);
    else if(request->argName(i) == "key")
      key = request->arg(i);
  }

  if(ssid == "") {
    message = "<h2>Ooops, no SSID...?</h2>\n<p>Looks like a bug :-(</p>";
  } else {
    char ssidchars[ssid.length()+1];
    char keychars[key.length()+1];
    ssid.toCharArray(ssidchars, ssid.length()+1);
    key.toCharArray(keychars, key.length()+1);
    WiFi.begin(ssidchars, keychars);
  }

  replacement_t repls[] = { // the elements to replace in the template
    { 1, apSSID.c_str() },
    { 7, title.c_str() },
    { 8, "" },
    { 9, message.c_str() },
  };
  String htmlPage = "";     // a String to hold the resultant page
  GET_HTML(htmlPage, templatePage, repls);

  request->send(200, "text/html", htmlPage);
}
void hndlStatus(AsyncWebServerRequest *request) { // UI to check connectivity
  dln(netDBG, "serving page at /status");

  String s = "";
  s += "<ul>\n";
  s += "\n<li>SSID: ";
  s += WiFi.SSID();
  s += "</li>";
  s += "\n<li>Status: ";
  switch(WiFi.status()) {
    case WL_IDLE_STATUS:
      s += "WL_IDLE_STATUS</li>"; break;
    case WL_NO_SSID_AVAIL:
      s += "WL_NO_SSID_AVAIL</li>"; break;
    case WL_SCAN_COMPLETED:
      s += "WL_SCAN_COMPLETED</li>"; break;
    case WL_CONNECTED:
      s += "WL_CONNECTED</li>"; break;
    case WL_CONNECT_FAILED:
      s += "WL_CONNECT_FAILED</li>"; break;
    case WL_CONNECTION_LOST:
      s += "WL_CONNECTION_LOST</li>"; break;
    case WL_DISCONNECTED:
      s += "WL_DISCONNECTED</li>"; break;
    default:
      s += "unknown</li>";
  }

  s += "\n<li>Local IP: ";     s += ip2str(WiFi.localIP());
  s += "</li>\n";
  s += "\n<li>Soft AP IP: ";   s += ip2str(WiFi.softAPIP());
  s += "</li>\n";
  s += "\n<li>AP SSID name: "; s += apSSID;
  s += "</li>\n";

  s += "</ul></p>";

  replacement_t repls[] = { // the elements to replace in the boilerplate
    { 1, apSSID.c_str() },
    { 7, "<h2>Status</h2>\n" },
    { 8, "" },
    { 9, s.c_str() },
  };
  String htmlPage = ""; // a String to hold the resultant page
  GET_HTML(htmlPage, templatePage, repls);

  request->send(200, "text/html", htmlPage);
}
void apListForm(String& f) { // utility to create a form for choosing AP
  const char *checked = " checked";
  int n = WiFi.scanNetworks();
  dbg(netDBG, "scan done: ");

  if(n == 0) {
    dln(netDBG, "no networks found");
    f += "No wifi access points found :-( ";
    f += "<a href='/'>Back</a><br/><a href='/wifi'>Try again?</a></p>\n";
  } else {
    dbg(netDBG, n); dln(netDBG, " networks found");
    f += "<p>Wifi access points available:</p>\n"
         "<p><form method='POST' action='wifichz'> ";
    delay(300);
    for(int i = 0; i < n; ++i) {
      f.concat("<input type='radio' name='ssid' value='");
      f.concat(WiFi.SSID(i));
      f.concat("'");
      f.concat(checked);
      f.concat(">");
      f.concat(WiFi.SSID(i));
      f.concat(" (");
      f.concat(WiFi.RSSI(i));
      f.concat(" dBm)");
      f.concat("<br/>\n");
      checked = "";
    }
    f += "<br/>Pass key: <input type='textarea' name='key'><br/><br/> ";
    f += "<input type='submit' value='Submit'></form></p>";

    cachedForm = f;
  }
}
String ip2str(IPAddress address) { // utility for printing IP addresses
  return
    String(address[0]) + "." + String(address[1]) + "." +
    String(address[2]) + "." + String(address[3]);
}
