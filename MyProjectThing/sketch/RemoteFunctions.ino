// This method does an action based on which button has been pressed

void updateRemoteButtons(char* newBrand);
void doRemoteAction(RemoteButton* RemoteButton, bool inManage);
void doRemoteFunctionUpdate();
const char* convertBrand(decode_type_t type);

void doRemoteAction(RemoteButton* remoteButton, bool inManage) {
  // Brand names of supported TVs
  char* LG_TITLE = "LG";
  char* SAMSUNG_TITLE = "Samsung"; 
  char* SHARP_TITLE = "Sharp"; 
  char* PANASONIC_TITLE = "Panasonic";
  // Get the title of the selected remoteButton
  char* title = remoteButton->getTitle();

  // If user wants to switch the brand of the remote
  if (title == LG_TITLE || title == SAMSUNG_TITLE || title == SHARP_TITLE || title == PANASONIC_TITLE) {
    updateRemoteButtons(title);
  } else { 
    // Otherwise do the command on the RemoteButton
    sendSignal(remoteButton);
  }
  // Redraw
  draw();
}
