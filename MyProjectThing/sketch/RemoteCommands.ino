//RemoteCommands.cpp
// A set of functions for sending IR signals from the remote.

/**
 * Sends an IR signal based on the remote button's selected brands
 */

void sendSignal(RemoteButton* remoteButton){
  char* brand = remoteButton->getBrand();
  unsigned long functionData = remoteButton->getData();

  if (brand=="Samsung"){
    sendSamsungSignal(functionData);
  }else if (brand=="LG" || brand=="Toshiba" || brand=="Technika"){
    sendNECSignal(functionData);
  }else if (brand=="Panasonic"){
    sendPanasonicSignal(functionData);
  }else if (brand=="Sharp"){
    sendSharpSignal(functionData);
  }
}

/*
 * Below are the functions for each type of device.
 * Some have specific protocols, such as panasonic's repeat
 */
void sendSamsungSignal(unsigned long dataToSend){
    irsend.sendSAMSUNG(dataToSend, 32);
    Serial.printf("Samsung");
    delay(100); // delay between each signal
}

void sendPanasonicSignal(unsigned long dataToSend){
  for (int i = 0; i < 3; i++) {
    irsend.sendPanasonic(0x4004,dataToSend);
    Serial.printf("Panasonic");
    delay(40);
  }
  delay(100); // delay between each signal
}

void sendNECSignal(unsigned long dataToSend){
  //LG does not need repeats
  irsend.sendNEC(dataToSend, 32);
  Serial.printf("NEC");
  delay(100); // delay between each signal
}

void sendSharpSignal(unsigned long dataToSend){
  //sendSharp function includes necessary repeats
  for (int i = 0; i < 3; i++) {
    irsend.sendDenon(0x824, 14);
    Serial.printf("Sharp");
  }
  delay(100); // delay between each signal
}
