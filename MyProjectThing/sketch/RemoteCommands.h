// RemoteCommands.h
// function prototypes and includes

#ifndef REMOTECOMMANDS_H
#define REMOTECOMMANDS_H

#include <IRremote.h>

IRsend irsend;
void sendSignal(RemoteButton*);
void sendSamsungSignal(unsigned long);
void sendPanasonicSignal(unsigned long);
void sendNECSignal(unsigned long);
void sendSharpSignal(unsigned long);
#endif
