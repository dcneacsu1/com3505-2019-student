// Draws main remote tiles on the unphone screen
void drawRemote(RemoteButton** RemoteButtons) {
  uint16_t startingY = border; // Initialise Y with a slight border
  uint16_t buttonHeight = HX8357_TFTHEIGHT / rows - border; // Calculate button height based on rows
  uint16_t buttonWidth = HX8357_TFTWIDTH / columns - border; // Calculate button width based on columns
  
  // Iterate through the rows and columns
  for (int i = 0; i < rows; i++) {
    uint16_t startingX = border; // Initialise X with a slight border
    for (int j = 0; j < columns; j++) {
      unPhone::tftp->setCursor(startingX + 40, startingY + 70); // Set cursor to bottom side of RemoteButton

      uint16_t colour = RemoteButtons[i*columns+j]->getColour(); // Get colour from RemoteButton
      unPhone::tftp->fillCircle(startingX + 70, startingY + 70, buttonWidth / 2, colour); // Draw a rectangle
      // Update text colour based on background colour
      if (colour == HX8357_WHITE || colour == HX8357_YELLOW || colour == HX8357_GREEN) {
        unPhone::tftp->setTextColor(HX8357_BLACK);
      } else {
        unPhone::tftp->setTextColor(HX8357_WHITE);
      }

      unPhone::tftp->print(RemoteButtons[i*columns+j]->getTitle()); // Print the title on the tile
      unPhone::tftp->setCursor(startingX + 10, startingY + 50);
      unPhone::tftp->print(RemoteButtons[i*columns+j]->getAdditionalText());
      
      startingX += buttonWidth + border; // Shift the x coordinate ready for the adjacent tile
    }
    startingY += buttonHeight + border; // Shift the y coordinate ready for the adjacent tile
  }
}
