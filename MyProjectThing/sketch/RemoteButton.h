// RemoteButton.h
// core definitions and includes

#ifndef REMOTEBUTTON_H
#define REMOTEBUTTON_H

#include "unphone.h"

// Class that holds all the properties and functions
// of a remote button
class RemoteButton {
    char* title;
    char* brand;
    char* additionalText;
    unsigned long functionData;
    bool functionOverridden;
    uint16_t colour;
    
  public:
    void init(char* titleC, char* brandC, uint16_t colourC) {
      title = titleC;
      brand = brandC;
      colour = colourC;
      additionalText = "";
    }
  
    char* getTitle();
    void setTitle(char*);

    char* getBrand();
    void setBrand(char*);

    uint16_t getColour();
    void setColour(uint16_t);
    
    unsigned long getData();
    void setData(unsigned long);

    char* getAdditionalText();
    void setAdditionalText(char*);
};

#endif
