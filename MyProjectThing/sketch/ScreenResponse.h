// ScreenResponse.h
// core definitions and includes

#ifndef SCREENRESPONSE_H
#define SCREENRESPONSE_H

// calibration data for converting raw touch data to the screen coordinates
#define TS_MINX 3800
#define TS_MAXX 100
#define TS_MINY 100
#define TS_MAXY 3750

//the multiplier for scaling LCD to touchscreen
uint16_t horizontalMultiplier = (TS_MINX - TS_MAXX) / HX8357_TFTWIDTH;
uint16_t verticalMultiplier = (TS_MAXY - TS_MINY) / HX8357_TFTHEIGHT;

//sizes of buttons 
uint16_t touchButtonHeight = (HX8357_TFTHEIGHT /4-border)*verticalMultiplier;
uint16_t touchButtonWidth = (HX8357_TFTWIDTH - border)*horizontalMultiplier;

void listenToTileList(uint16_t x, uint16_t y, RemoteButton** RemoteButtons, bool inManage);
#endif
