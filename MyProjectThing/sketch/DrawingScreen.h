// DrawingScreen.h
// core definitions and includes

#ifndef DRAWINGSCREEN_H
#define DRAWINGSCREEN_H


// The number of columns to display when displaying tiles
const uint8_t columns = 2;
// The number of rows to display when displaying tiles
const uint8_t rows = 3;

uint8_t listRows = 4; // The number of items to display when viewing items in list form.
uint8_t border = 8; // The border to be given when rendering anything
uint16_t buttonHeight = HX8357_TFTHEIGHT / listRows - border; // The height of list buttons
uint16_t buttonWidth = HX8357_TFTWIDTH - border; // The width of list buttons

// This method draws the main remote tiles on the screen
void drawRemote(RemoteButton**);

#endif
