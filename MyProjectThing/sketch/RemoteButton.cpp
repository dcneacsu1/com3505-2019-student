// RemoteButton.cpp
// core library

#include "RemoteButton.h"
#include "IRCodes.h"

//getters and setters for the Title field.
//This field is essentially the button function
char* RemoteButton::getTitle() {
  return title;
}
void RemoteButton::setTitle(char* value) {
  title = value;
  if (title != "Custom" && title != "Media") {
    functionOverridden = false; // Clear override if a default title has been selected
  }
}

// Optional field which adds text above the title on the button
char* RemoteButton::getAdditionalText() {
  return additionalText;
}

void RemoteButton::setAdditionalText(char* value) {
  additionalText = value;
}

//getters and setters for the brand
//this is the make that defines the sending protocol
//for the IR
char* RemoteButton::getBrand() {
  return brand;
}
void RemoteButton::setBrand(char* value) {
  brand = value;
}

//getters and setters for the colour
uint16_t RemoteButton::getColour() {
  return colour;
}
void RemoteButton::setColour(uint16_t value) {
  colour = value;
}

/* getter and setter for the button IR signal
 * It uses IRCodes.cpp, along with the 
 * brand and title to get the correct function code
 * for the function currently occupying this RemoteButton
 */
unsigned long RemoteButton::getData() {
  if (functionOverridden) {
    return functionData;
  }
  
  if (title=="Power"){
    return getPowerCode(brand);
  }
  else if (title=="Vol up"){
    return getVolUpCode(brand);
  }
  else if (title=="Vol down"){
    return getVolDownCode(brand);
  }
  else if (title=="Chan up"){
    return getChanUpCode(brand);
  }
  else if (title=="Chan down"){
    return getChanDownCode(brand);
  }

}

void RemoteButton::setData(unsigned long value) {
  functionData = value;
  functionOverridden = true;
}
